import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { throwMatDuplicatedDrawerError } from '@angular/material/sidenav';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'respirator';
  positionN;
  result;
  messages=[];
  positions = [
    { value: 'N', name: 'nord' },
    { value: 'E', name: 'east' },
    { value: 'W', name: 'west' },
    { value: 'S', name: 'sud' },
  ];

  // positions = ['N','E','W','S'];
  possitionX;
  possitionY;
  direction='';
  form: FormGroup = new FormGroup({});
  constructor(
    private fb: FormBuilder,
  ) {
    this.form = fb.group({
      dimessionX: ['', [Validators.required]],
      dimessionY: ['', [Validators.required]],
      positionX: ['', [Validators.required]],
      positionY: ['', [Validators.required]],
      orientation:['', [Validators.required]],
      instruction: ['', [Validators.required]]
    });
  }

  submit(){
    this.possitionX = this.form.value.positionX;
    this.possitionY = this.form.value.positionY;
    this.positionN = this.positions.indexOf(this.form.value.orientation);
    console.log(this.positions.indexOf(this.form.value.orientation));

    let directionList = this.form.value.instruction.toUpperCase().split('');
    for(let i=0 ; i<directionList.length;i++){
      if(directionList[i]==='D'){
        this.positionN= (this.positionN+1)%4;
        this.messages.push('tourner à droite')
      }
      else if (directionList[i]==='G'){
        this.positionN= (this.positionN-1)%4;
        this.messages.push('tourner à gauche')

      }
      else {
        switch(this.positions[this.positionN].value) { 
          case 'N': { 
            this.possitionY =this.possitionY+1;
            break; 
          } 
          case 'S': { 
            this.possitionY =this.possitionY-1; 
            break; 
            } 
            case 'E': { 
              this.possitionX =this.possitionX-1;
              break; 
            }
            case 'W': { 
              this.possitionX =this.possitionX+1;
              break; 
            } 
          }
          this.messages.push('marche en avant')

      }
    }
  this.direction = this.positions[this.positionN].value;
  this.result= 'position x=' + this.possitionX + ' ** position y =  '
    + this.possitionY + ' **  Direction est ' +
    this.positions[this.positionN].name;
  }
}
